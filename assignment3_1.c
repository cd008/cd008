//1. Develop a C program to find the possible roots of quadratic equation using switch case statement.
#include<stdio.h>
#include<math.h>
int main() {
int a,b,c,x;
float r1,r2,D;
printf("Enter values of a,b,c \n");
scanf("%d %d %d",&a,&b,&c);
D=(b*b)-(4*a*c);

if(D>0)
x=1;
else if(D==0)
x=2;
else if(D<0)
x=3;

switch(x) {
case 1:
 r1=(-b+sqrt(D))/(2*a);
 r2=(-b-sqrt(D))/(2*a);
 printf("Real and distinct roots r1=%f and r2=%f",r1,r2);
 break;
 
case 2:
 r1=(-b)/(2*a);
 printf("Real and equal roots r1=r2=%f",r1);
 break;
 
case 3:
 printf("Imaginary roots");
 break;
}
return 0;
}
 
