/*
b) Consider an array called odd[10] representing odd numbers from 1 to 15(in order) with elements of the array as follows:
1 3 5 7 9 11 13 15
Write a program to delete the missed element 5 in the above array. */

#include<stdio.h>
int main() {
int i, odd[10]={1,3,5,7,9,11,13,15};
for(i=2; i<=7; i++)
odd[i]=odd[i+1];
for(i=0; i<=6; i++)
printf("%d",odd[i]);
return 0;
}