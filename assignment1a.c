//Write a program to find the largest of three numbers using ternary operator.
#include<stdio.h>
int main()
{ 
    int a,b,c,large;
    printf("Input three numbers \n");
    scanf("%d%d%d",&a,&b,&c);

    large=(a>b)?((a>c)?a:c):((b>c)?b:c);
    printf("The largest number is %d \n",large);

    return 0; 

}
