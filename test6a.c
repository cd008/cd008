/*Develop a program to find average of n numbers
a) with arrays*/
#include<stdio.h>
int main() {
	int i, n, a[100], sum=0;
    float avg;

    printf("Enter size of array \n");
    scanf("%d",&n);
    printf("Enter the numbers \n");
    for(i=0; i<n; i++)
        scanf("%d",&a[i]);

    for(i=0; i<n; i++)
        sum=sum+a[i];
    avg=sum/n;

    printf("The average is %f",avg);
    return 0;
}
