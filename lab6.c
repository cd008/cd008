//Demonstrate reading a two-dimensional array marks which stores marks of 5 students in 3 subjects and display the highest marks in each subject.	
#include<stdio.h>
int main()
{
    int arr[5][3],i,j,max;
    for(i=0;i<5;i++)
    {
        printf("\nenter the marks of the student %d\n",i);
        for(j=0;j<3;j++)
        {
                printf("\n marks[%d][%d]: ",i,j);
                scanf("%d",&arr[i][j]);
        }
    }
    for(j=0;j<3;j++)
    {
        max=arr[0][j];
        for(i=1;i<5;i++)
        {
            if(arr[i][j]>max)
            {
                max=arr[i][j];
                printf("\n the highest marks in subject %d=%d",j,max);
            }
        }
    }
return 0;
}
