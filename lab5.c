//Implement and execute a program to interchange the largest and smallest number in the given array.
#include<stdio.h>
int main() {
	int a[100], n, small, small_pos, large, large_pos, i, temp=0;

	printf("Enter size of array \n");
	scanf("%d",&n);
	printf("Enter elements of the array \n");
	for(i=0; i<n; i++)
		scanf("%d",&a[i]);

	small=a[0];
	small_pos=0;
	large=a[0];
	large_pos=0;

	for(i=0; i<n; i++) {
		if(a[i]<small) {
			small=a[i];
			small_pos=i;
		}

		if(a[i]>large){
			large=a[i];
			large_pos=i;
		}
	}

	temp=small;
	small=large;
	large=temp;

	a[large_pos]=large;
	a[small_pos]=small;

	for(i=0; i<n; i++)
		printf("%d \t",a[i]);

	return 0;
}





