//Develop a C program to find area of a circle using functions
#include<stdio.h>
float input() {
float r;
printf("Enter radius of circle \n");
scanf("%f",&r);
return r;
}

float area(float r) {
float area;
area=3.1415*r*r;
return area;
}

void display(float a) {
printf("The area of circle is %f \n",a);
}

int main() {
float radius, ar;
radius=input();
ar=area(radius);
display(ar);
return 0;
}
