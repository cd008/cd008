//Develop a C program to find all possible roots of a quadratic equations
#include<stdio.h>
#include<math.h>
int input() {
	int i;
	printf("Enter coefficient \n");
	scanf("%d",&i);
	return i;
}

void roots(a,b,c) {
	float D,r1,r2;
	D=(b*b)-(4*a*c);
	if(D>0)
	{ 
		r1=(-b+sqrt(D))/(2*a);
			r2=(-b-sqrt(D))/(2*a);
			printf("Real and Distinct roots r1=%f and r2=%f \n",r1,r2);
	}
	else if(D==0)
	{
		r1=-b/(2*a);
		printf("Real and Equal roots r1=r2=%f \n",r1);
	}
	else if(D<0)
	{
		printf("Roots are imaginary \n");
	}
}

int main() {
	int a,b,c;
	a=input();
	b=input();
	c=input();
	roots(a,b,c);	
	return 0;
}
