//2) Write a program to compute sum of series 1/1^2 + 1/2^2 +...+1/n^2

#include<stdio.h>
#include<math.h>
    float series(int n)
{
        float sum=0.0,ser;
        int i;
        for(i=1;i<=n;i++)
        {
            ser=1/pow(i,i);
            sum=sum+ser;
        }
    return sum;
}

int main()
{
    int n;
    float d;
    printf("enter a number\n");
    scanf("%d",&n);
    d=series(n);
    printf("sum of the series is: %f",d);
return 0;
}


