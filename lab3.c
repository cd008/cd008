//Develop a C program to convert time (in hours and minutes) to minutes using functions 
#include<stdio.h>
int input() {
int t;
printf("Enter time \n");
scanf("%d",&t);
return t;
}

int time(int h,int m) {
int t;
t=m+h*60;
return t;
}

void display(int t) {
printf("Time in minutes is %d \n",t);
}

int main() {
int h,m,t;
h=input();
m=input();
t=time(h,m);
display(t);
return 0;
}