//2. Write a program to find area of a triangle using functions.
#include<stdio.h>
#include<math.h>
float input() {
    float a; 
    printf("Enter the side length \n");
    scanf("%f",&a);
    return a;
}

float area(float a,float b,float c) {
    float s, area;
    s=(a+b+c)/2; 
    area=sqrt(s*(s-a)*(s-b)*(s-c));
    return area;
}

void display(float area) {
    printf("The area of the triangle is %f \n",area);
}

int main() {
    float a,b,c;
    float ar;
    a=input();
    b=input();
    c=input();
    ar=area(a,b,c);
    display(ar);  

    return 0;
}
