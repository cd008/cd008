/*Develop a program to find average of n numbers
b) without arrays. */
#include<stdio.h>
int main() {
    int n,a,i,sum=0;
    float avg;
    printf("Enter the number of values \n");
    scanf("%d",&n);
    for(i=1; i<=n; i++) {
        printf("Enter number \n");
        scanf("%d",&a);
        sum=sum+a;
    }

    avg=sum/n;
    printf("Average is %f",avg);
    return 0;
}