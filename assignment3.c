//Write a program to swap two numbers without using a temporary variable

#include<stdio.h>
int main()
{ 
	int a,b;
	printf("Enter value of first number \n");
	scanf("%d",&a);
	printf("Enter value of second number \n");
	scanf("%d",&b);
	
	a=a+b;
	b=a-b;
	a=a-b; 
	
	printf("The two numbers after swapping are %d and %d",a,b);
	return 0;
	
}
 
