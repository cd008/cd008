//1. A company decides to give bonus to all its employees on Diwali. A 5% bonus on salary is given to the male workers and 10% bonus on salary to the female workers. Write a program to enter the salary and sex of the employee. If the salary employee is less than Rs 10,000 then the employee gets an extra 2% bonus on salary. Calculate the bonus that has to be given to the employee and display the salary that the employee will get.
#include<stdio.h>
int main()
{
 int salary,gender,bonus;
 printf("Enter salary \n");
 scanf("%d",&salary);
 printf("If female enter 1, if male enter 0 \n");
 scanf("%d",&gender);
 
 if(gender==1 && salary<10000)
 {
  bonus=0.1*salary+0.02*salary;
  salary=salary+bonus;
  printf("Bonus = %d and new salary = %d \n",bonus,salary);
 }
 else if(gender==1 && salary>=10000)
 {
 bonus=0.1*salary;
 salary=salary+bonus;
 printf("Bonus = %d and new salary = %d \n",bonus,salary);
 }
 
  if(gender==0 && salary<10000)
 {
  bonus=0.05*salary+0.02*salary;
  salary=salary+bonus;
  printf("Bonus = %d and new salary = %d \n",bonus,salary);
 }
 else if(gender==0 && salary>=10000)
 {
 bonus=0.05*salary;
 salary=salary+bonus;
 printf("Bonus = %d and new salary = %d \n",bonus,salary);
 }
 
return 0;
}
 
 