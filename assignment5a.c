/*a) Consider an array called even[10] representing even numbers from 2 to 16(in order) with elements of the array as follows:
2 4 6 8 12 14 16
Write a program to store the missed element 10 in the above array.*/

#include<stdio.h>
int main() {
int i, even[10]={2,4,6,8,12,14,16};
for(i=6;i>=4;i--)
even[i+1]=even[i];
even[4]=10;
for(i=0;i<=7;i++)
printf("%d",even[i]);
return 0;
}
