//Develop a C program to print multiples of n from 1 to 100
#include<stdio.h>
int input() {
	int t;
	printf("Enter a number \n");
	scanf("%d",&t);
	return t;
}

void multiples(t) {
	for(int i=1;i<=100;i++) {
		if(i%t==0)
			printf("%d \t",i);
	}
}

int main() {
	int n;
	n=input();
	multiples(n);
	return 0;
}
